Algoritmo Ejercicio3
    Definir distancia, velocidad, tiempo Como Real
    
    Escribir "Ingrese la distancia entre las dos ciudades en kilómetros:"
    Leer distancia
    
    Escribir "Ingrese la velocidad promedio en kilómetros por hora:"
    Leer velocidad
    
    tiempo <- distancia / velocidad
    
    Escribir "El tiempo estimado en llegar de una ciudad a otra en bicicleta es:", tiempo, "horas"
FinAlgoritmo
