//RESPUESTA A LA PREGUNTA 15 RESPUESTA "B"
Algoritmo Ejercicio15B
    Definir dias, A�os, Meses, Semanas, los_demas_dias Como Entero
    
    Escribir "Ingrese el n�mero de d�as: "
    Leer dias
    
    A�os <- dias / 365
    los_demas_dias <- dias MOD 365
    
    Si los_demas_dias >= 30 Entonces
        meses <- los_demas_dias/ 30
        los_demas_dias <- los_demas_dias MOD 30
    FinSi
    
    Si los_demas_dias >= 7 Entonces
        semanas <- resto_dias / 7
        los_demas_dias <- los_demas_dias MOD 7
    FinSi
    
    Escribir "A�os: ", A�os
    Escribir "Meses: ", Meses
    Escribir "Semanas: ", Semanas
    Escribir "D�as: ", los_demas_dias
    
FinAlgoritmo
