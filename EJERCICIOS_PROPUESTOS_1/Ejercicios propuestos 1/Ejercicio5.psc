Algoritmo Ejercicio5
	Definir R, H Como Real
	Definir Area, Volumen como real 
	
	Escribir "Ingrese el radio del cilindro:"
    Leer R
	
	Escribir "Ingrese la altura del cilindro:"
    Leer H
	Area <- 2 * 3.1416 * R * (R + H)
    Volumen <- 3.1416 * R^2 * H
	Escribir "El �rea del cilindro es:", Area
    Escribir "El volumen del cilindro es:", Volumen
FinAlgoritmo
