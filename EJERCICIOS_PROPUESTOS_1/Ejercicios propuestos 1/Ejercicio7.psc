Algoritmo Ejercicio7
    // Solicitar al usuario que ingrese la calificación numérica
    Escribir "Ingrese la calificación numérica (entre 1 y 20):"
    Leer calificacion
	
    // Determinar la letra correspondiente a la calificación
    Si calificacion >= 19 Entonces
        letra = "A"
    Sino Si calificacion >= 16 Entonces
			letra = "B"
	Sino Si calificacion >= 13 Entonces
				letra = "C"
	Sino Si calificacion >= 10 Entonces
					letra = "D"
	Sino
					letra = "E"
	FinSino
				
				// Mostrar la letra correspondiente a la calificación	
				Escribir "La calificación", calificacion, "equivale a la letra", letra
				
FinAlgoritmo

