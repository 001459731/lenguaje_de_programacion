//RESPUESTA A LA PREGUNTA A 
Algoritmo Ejercicio15
    Definir dias, a�os, meses, semanas, los_demas_dias Como Entero
    
    Escribir "Ingrese el n�mero de d�as: "
    Leer dias
    
    a�os <- dias / 365
    los_demas_dias <- dias MOD 365
    
    meses <- los_demas_dias / 30
    los_demas_dias <- los_demas_dias MOD 30
    
    semanas <- los_demas_dias / 7
    los_demas_dias <- los_demas_dias MOD 7
    
    Escribir "A�os: ", a�os
    Escribir "Meses: ", meses
    Escribir "Semanas: ", semanas
    Escribir "D�as: ", los_demas_dias
    
FinAlgoritmo
