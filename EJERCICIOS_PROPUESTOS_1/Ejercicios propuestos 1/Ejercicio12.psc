Algoritmo Ejercicio12
    Definir cantidad_numeros, i Como Entero
    Definir suma, numero, media Como Real
    
    Escribir "Ingrese la cantidad de n�meros: "
    Leer cantidad_numeros
    
    suma <- 0
    
    Para i <- 1 Hasta cantidad_numeros Con Paso 1 Hacer
        Escribir "Ingrese el n�mero ", i, ": "
        Leer numero
        suma <- suma + numero
    FinPara
    
    media <- suma / cantidad_numeros
    
    Escribir "La media aritm�tica de los n�meros ingresados es: ", media
    
FinAlgoritmo
