Algoritmo Ejercicio14
    Definir precio_producto, cantidad, total_a_pagar Como Real
    
    Escribir "Ingrese el precio del producto: "
    Leer precio_producto
    
    Escribir "Ingrese la cantidad de productos: "
    Leer cantidad
    
    total_a_pagar <- precio_producto * cantidad
    
    Si total_a_pagar >= 1000 Entonces
        Descuento <- total_a_pagar * 0.1
        total_a_pagar <- total_a_pagar - descuento
        Escribir "Total a pagar con descuento del 10%: ", total_a_pagar
    Sino
        Escribir "Total a pagar: ", total_a_pagar
    FinSi
    
FinAlgoritmo
