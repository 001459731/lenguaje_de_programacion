//RESPUESTA A LA PREGUNTA B 
Algoritmo Ejercicio13
    Definir num_obreros, i Como Entero
    Definir salario_total, salario_obrero Como Real
    
    num_obreros <- 5
    salario_total <- 0
    
    Para i <- 1 Hasta num_obreros Con Paso 1 Hacer
        Escribir "Ingrese el salario del obrero ", i, ": "
        Leer salario_obrero
        
        salario_total <- salario_total + salario_obrero
    FinPara
    
    Escribir "El salario total a pagar es: ", salario_total
    
FinAlgoritmo

//RESPUESTA A LA PREGUNTA A
//�Que pasaria si no se decrementa al n�meros de obreros en uno?
//Si no decrementamos el n�mero de obreros en uno dentro del bucle, el bucle se ejecutar�a 
//infinitamente ya que la condici�n Mientras nunca se volver�a falsa. Esto se traduce en un bucle
//infinito que podr�a causar que el programa se bloquee o se vuelva no responsive.