Algoritmo Ejercicio10
    Definir cantidad_entradas, costo_entrada, descuento, total_a_pagar Como Real
    
    Escribir "Ingrese la cantidad de entradas a comprar (m�ximo 4): "
    Leer cantidad_entradas
    
    Si cantidad_entradas = 1 Entonces
        costo_entrada <- 10
    Sino
        Si cantidad_entradas = 2 Entonces
            costo_entrada <- 10 * 2
            descuento <- costo_entrada * 0.10
        Sino
            Si cantidad_entradas = 3 Entonces
                costo_entrada <- 10 * 3
                descuento <- costo_entrada * 0.15
            Sino
                Si cantidad_entradas = 4 Entonces
                    costo_entrada <- 10 * 4
                    descuento <- costo_entrada * 0.20
                FinSi
            FinSi
        FinSi
    FinSi
    
    total_a_pagar <- costo_entrada - descuento
    
    Escribir "El total a pagar es: ", total_a_pagar
    
FinAlgoritmo
