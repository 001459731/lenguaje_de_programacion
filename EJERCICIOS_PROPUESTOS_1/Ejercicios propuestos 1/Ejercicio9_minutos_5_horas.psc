Algoritmo Ejercicio9_minutos_5_horas
    Definir horas, minutos_totales Como Entero
    
    horas <- 5
    minutos_por_hora <- 60
    
    minutos_totales <- horas * minutos_por_hora
    
    Escribir "En 5 horas hay ", minutos_totales, " minutos."
    
FinAlgoritmo
