Algoritmo Ejercicio6
    Definir N como Entero
    
    Escribir "Ingrese un n�mero:"
    Leer N
    
    Si N % 2 = 0 Entonces
        Escribir "El n�mero ", N, " es par."
    Sino
        Escribir "El n�mero ", N, " es impar."
    FinSi
    
FinAlgoritmo
