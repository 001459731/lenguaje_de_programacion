Algoritmo Ejercicio7_CASE
Definir nuevaClasificacion Como Caracter
Definir nota Como Entero

Escribir "Por favor, introduce la nota obtenida:"
leer nota

Segun nota Hacer
	Caso 19, 20:
		nuevaClasificacion <- "A"
	Caso 16, 17, 18:
		nuevaClasificacion <- "B"
	Caso 13, 14, 15:
		nuevaClasificacion <- "C"
	Caso 10, 11, 12:
		nuevaClasificacion <- "D"
	Caso 1, 2, 3, 4, 5, 6, 7, 8, 9:
		nuevaClasificacion <- "E"
	De Otro Modo:
		nuevaClasificacion <- "No clasificado"
FinSegun

Escribir "La clasificación correspondiente es:", nuevaClasificacion
FinAlgoritmo
