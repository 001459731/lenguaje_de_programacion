Algoritmo Ejercicio16
    Definir N, BC, BV, BD, BC2, BM, Resto Como Entero
    
    Escribir "Ingrese el valor monetario para diferenciarlo: "
    Leer N
    
    BC <- 0
    BV <- 0
    BD <- 0
    BC2 <- 0
    BM <- 0
    
    Si N >= 50000 Entonces
        BC <- BC + 1
        N <- N - 50000
    FinSi
    
    Si N >= 20000 Entonces
        BV <- BV + 1
        N <- N - 20000
    FinSi
    
    Si N >= 10000 Entonces
        BD <- BD + 1
        N <- N - 10000
    FinSi
    
    Si N >= 5000 Entonces
        BC2 <- BC2 + 1
        N <- N - 5000
    FinSi
    
    Si N >= 1000 Entonces
        BM <- BM + 1
        N <- N - 1000
    FinSi
    
    Resto <- N
    
    Escribir "Diferencie del valor monetario:"
    Escribir "Billetes de 50000: ", BC
    Escribir "Billetes de 20000: ", BV
    Escribir "Billetes de 10000: ", BD
    Escribir "Billetes de 5000: ", BC2
    Escribir "Monedas de 1000: ", BM
    Escribir "Resto: ", Resto
    
FinAlgoritmo