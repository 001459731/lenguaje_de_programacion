//RESPUESTA A LA PREGUNTA 13 "C"
Algoritmo Ejercicio_13c
    Definir num_obreros, i Como Entero
    Definir salario_total, salario_obrero Como Real
    
    num_obreros <- 5
    salario_total <- 0
    i <- 1
    
    Repetir
	Escribir "Ingrese el salario del obrero ", i, ": "
	Leer salario_obrero
	
	salario_total <- salario_total + salario_obrero
	i <- i + 1
    Hasta que i > num_obreros

Escribir "El salario total a pagar es: ", salario_total

FinAlgoritmo
